/**
 * Fibonacci Test Module
 */
import { describe, test ,expect} from '@jest/globals';

function fibonacci(number) {
    /**
     * The function calculates the nth number in the Fibonacci sequence using recursion.
     */
    if (number === 0) {
        return 0;
    }
    if (number === 1) {
        return 1;
    }
    return fibonacci(number - 1) + fibonacci(number - 2);
}

describe('Fibonacci Test', () => {
    test('returns 0 when passed 0 as an argument', () => {
        expect(fibonacci(0)).toBe(0);
    });

    test('returns 1 when passed 1 as an argument', () => {
        expect(fibonacci(1)).toBe(1);
    });

    test('returns correct output for small input values', () => {
        expect(fibonacci(2)).toBe(1);
        expect(fibonacci(3)).toBe(2);
        expect(fibonacci(4)).toBe(3);
    });

    test('returns correct output for large input values', () => {
        expect(fibonacci(10)).toBe(55);
        expect(fibonacci(20)).toBe(6765);
    });
});
