function findMatchingPair(numbers, targetSum) {
    let leftPointer = 0;
    let rightPointer = numbers.length - 1;

    while (leftPointer < rightPointer) {
        const currentSum = numbers[leftPointer] + numbers[rightPointer];

        if (currentSum === targetSum) {
            return [numbers[leftPointer], numbers[rightPointer]];  // Matching pair found
        } else if (currentSum < targetSum) {
            leftPointer++;  // Move left pointer to the right
        } else {
            rightPointer--;  // Move right pointer to the left
        }
    }

    return null;  // No matching pair found
}

// Example Usage:
const numbers1 = [2, 3, 6, 7];
const targetSum1 = 9;
const result1 = findMatchingPair(numbers1, targetSum1);
// result1 should be [3, 6]

const numbers2 = [1, 3, 3, 7];
const targetSum2 = 9;
const result2 = findMatchingPair(numbers2, targetSum2);
console.log("---------point number 4-------------")
console.log("result [2, 3, 6, 7]:"+ result1)
console.log("result [1, 3, 3, 7]:"+ result2)
console.log("--------point number 4-----------")

// result2 should be null (no matching pair)
