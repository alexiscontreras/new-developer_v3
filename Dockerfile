# Use an official Node.js runtime as a parent image
FROM node:20

# Set the working directory to /app
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install any needed packages specified in package.json
RUN npm install

# Bundle app source
COPY . .

# Run ESLint on your JavaScript files
CMD ["bash", "-c", "npx eslint -- **/*.js && npm run test -- **/*.test  && npm run -- answer4 && npm run -- answer5"]

