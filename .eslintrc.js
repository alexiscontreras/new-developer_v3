module.exports = {
    env: {
      node: true,
      commonjs: true,
      es2023: true,
    },
    extends: 'eslint:recommended',
    parserOptions: {
      ecmaVersion: 12,
      "sourceType": "module"
    },
    rules: {
        
      // Agrega tus reglas aquí
    },
  };
  